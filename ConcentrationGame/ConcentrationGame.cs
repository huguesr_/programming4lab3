using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("ConcetrationTests")]
namespace MemoryGame
{
    public class ConcentrationGame
    {

        private ConcentrationGrid _grid;
        private Player _activePlayer;
        private Player _inactivePlayer;
        public static void Main(string[] args)
        {
            ConcentrationGame game = new ConcentrationGame();
            game.RunGame();
        }


        //The constructor sets up a new game of Concentration
        public ConcentrationGame()
        {
            _grid = new ConcentrationGrid();
            Console.WriteLine("Welcome to Concentration, the matching game!");
            Console.WriteLine("Player 1, enter your name:");
            _activePlayer = new Player(GetValidInput());
            Console.WriteLine("Player 2, enter your name:");
            _inactivePlayer = new Player(GetValidInput());
        }

        //Get non-null input
        private string GetValidInput()
        {
            string? input;
            do
            {
                input = Console.ReadLine();
            } while (input == null);
            return input;
        }

        private int GetValidGuess(){
            Boolean valid = false;
            int input = 0;
            while(!valid){
                try{
                    input = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception e){
                    input = 7;
                }
                if(input <= 3 && input >= 0){
                    valid = true;
                }
                else{
                    Console.Write("Invalid input: Enter int between 0-3\n");
                }
            }
            return input;
        }


        private void RunGame()
        {
            while (!_grid.IsEmpty())
            {
                Boolean madeMatch = DoTurn(_activePlayer);
                if (madeMatch)
                {
                    _activePlayer.addPoint();
                    Console.WriteLine($"{_activePlayer.Name} made a match! They now have {_activePlayer.Points} points");
                }
                else
                {
                    Console.WriteLine($"Sorry, no match! It is now {_inactivePlayer.Name}'s turn.");
                    SwitchActivePlayer();

                }
                // Console.ReadKey();
                Console.WriteLine("Press enter to continue");
                Console.ReadLine();
            }
            ShowResults();
        }

        internal Boolean DoTurn(Player p)
        {
            //Create List of Guesses;
            List<Tuple<int, int>> guesses = new List<Tuple<int, int>>();
            //Ask for first guess, add it to the list
            do{
                guesses.Clear();
                guesses.Add(GetGuess(p, guesses));
                //Ask for second guess, add it to the list
                guesses.Add(GetGuess(p, guesses));
                if(guesses[0].Equals(guesses[1])){
                    Console.WriteLine("Can't guess same thing");
                }
            }while(guesses[0].Equals(guesses[1]));
            Console.Clear();
            _grid.PrintGrid(guesses);
            //Check to see if it Matches, return the result
            return _grid.CheckMatch(guesses[0], guesses[1]);
        }

        //Prompt the user to enter a guess.
        internal Tuple<int, int> GetGuess(Player p, List<Tuple<int, int>> guesses)
        {
            Console.Clear();
            Console.WriteLine($"{p.Name}, choose a row and column to guess");
            _grid.PrintGrid(guesses);
            Console.Write("Row: ");
            int row = GetValidGuess();
            Console.Write("Col: ");
            int column = GetValidGuess();
            return new Tuple<int, int>(row, column);
        }

        //Swaps the currently active player, making the inactive player active and vice versa
        internal void SwitchActivePlayer(){
            Player TempPlayer = _inactivePlayer;
            _inactivePlayer = _activePlayer;
            _activePlayer = TempPlayer;
        }

        private void ShowResults()
        {
            Console.Clear();
            Console.WriteLine($"Game over!");
            Console.WriteLine($"{_activePlayer.Name} has {_activePlayer.Points} points");
            Console.WriteLine($"{_inactivePlayer.Name} has {_inactivePlayer.Points} points");
            if (_activePlayer.Points > _inactivePlayer.Points)
            {
                Console.WriteLine($"{_activePlayer.Name} wins!");
            }
            else
            {
                Console.WriteLine($"{_inactivePlayer.Name} wins!");
            }
        }
    }
}