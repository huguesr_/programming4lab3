using MemoryGame;
namespace ConcentrationTests;

[TestClass]
public class TestGame
{
    [TestMethod]
    // This unit test checks if the deck generates a pair of each card
    public void UniquePairs() {
        // Arrange
        ConcentrationGrid game = new();
        List<Card> Deck = game.CreateCards();
        List<Card> GoodPair = new List<Card> {
            new Card(CardValue.A), new Card(CardValue.A),
            new Card(CardValue.B), new Card(CardValue.B),
            new Card(CardValue.C), new Card(CardValue.C),
            new Card(CardValue.D), new Card(CardValue.D),
            new Card(CardValue.E), new Card(CardValue.E),
            new Card(CardValue.F), new Card(CardValue.F),
            new Card(CardValue.G), new Card(CardValue.G),
            new Card(CardValue.H), new Card(CardValue.H)};
        // Act
        game.FillGrid(Deck);
        // Assert
        CollectionAssert.AreEqual(Deck,GoodPair);
    }

    [TestMethod]
    // When grid is full, check if method return false
    public void FullGridNotEmpty(){
        // Arrange
        ConcentrationGrid game = new();

        // Act
        List<Card> Deck = game.CreateCards();
        game.FillGrid(Deck);

        // Assert
        Assert.IsFalse(game.IsEmpty());
    }

    // Where grid is empty as well, method return true
    [TestMethod]
    public void EmptyGridIsEmpty() {
        // Arrange
        ConcentrationGrid game = new ConcentrationGrid();

        // Act
        List<Card> deck = game.CreateCards();
        game.FillGrid(deck); 
  
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                game.CheckMatch(new Tuple<int, int>(i, j), new Tuple<int, int>(i, j));
            }
        }

        // Assert
        Assert.IsTrue(game.IsEmpty()); // Making sure the grid is empty after removing all cards
    }
    
    // Unit test where grid has one empty row, but the rest is full, should return false
    [TestMethod]
    public void CheckHalfFullGrid(){
        // Arrange
        ConcentrationGrid game = new ConcentrationGrid();

        // Act
        List<Card> deck = game.CreateCards();
        game.FillGrid(deck); 

        game.CheckMatch(new Tuple<int,int>(0,0),new Tuple<int,int>(0,1));
        game.CheckMatch(new Tuple<int,int>(0,2),new Tuple<int,int>(0,3));

        // Assert
         Assert.IsFalse(game.IsEmpty());
    }
    // Checks the match of two valid card, returns true
    [TestMethod]
    public void CheckRightMatch(){
        // Arrange
        ConcentrationGrid game = new();
        // Act
        List<Card> Deck = game.CreateCards();
        game.FillGrid(Deck);

        // Assert
        Assert.IsTrue(game.CheckMatch(new Tuple<int,int>(0,0),new Tuple<int,int>(0,1)));
    }

    // Checks for wrong match, returns false
    [TestMethod]
    public void CheckWrongMatch(){
        // Arrange
        ConcentrationGrid game = new();

        // Act
        List<Card> Deck = game.CreateCards();
        game.FillGrid(Deck);

        // Assert
        Assert.IsFalse(game.CheckMatch(new Tuple<int,int>(0,0),new Tuple<int,int>(0,2)));
    }

    // This method will check if the nulls can be matched, should return false
    [TestMethod]
    public void CheckNullMatch(){
        // Arrange
        ConcentrationGrid game = new();

        // Act
        List<Card> Deck = game.CreateCards();
        game.FillGrid(Deck);
        // This is to make cards null
        game.CheckMatch(new Tuple<int,int>(0,0),new Tuple<int,int>(0,1));

        // Assert
        Assert.IsFalse(game.CheckMatch(new Tuple<int,int>(0,0),new Tuple<int,int>(0,1)));
    }
}