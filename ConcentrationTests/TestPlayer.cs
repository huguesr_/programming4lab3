using MemoryGame;
namespace MemoryGame;

[TestClass]
public class PlayerTest{
    [TestMethod]
    // Checking a player starts with 0 points
    public void NewPlayerTest(){
        // Arrange
        Player newP = new Player("cj");
        // Assert
        Assert.AreEqual(0, newP.Points);
    }

    // Somehow check if the point is incrementing
}